Whalesburg contracts audit
==========================

The audit is performed by Denis Glotov, https://glotov.org on May 31, 2018,
for Whalesburg s.r.o, https://whalesburg.com/.

Published on June 3, 2018.


Scope
=====

This audit reviews contracts from the repository [WLproject] at commit
`25ec4517af959d9df1c888cabf729a98c69a32a5` (master at the time of the review).

The following files have been reviewed:

1.  `contracts/WhalesburgSale.sol`,
    sha3: `07cd2b189c10f81044d34e79b0481f6fce352c5a1ad953894a4c3cfb`

2.  `contracts/WhalesBurgToken.sol`,
    sha3: `197c6f13f4e2774f7c980c9c0478d392f3fa25df498a558c5f686b91`

3.  `contracts/VestingConstructor.sol`,
    sha3: `1fea4d27b067932301a89d51cb56879d45808256dc6cc6c496bd431a`

4.  `contracts/Migrations.sol`,
    sha3: `46f631a02de1426784e71202e7e277032cd7cb58dc0145327a545bf2`

    The Migrations contract is a helper contract for truffle framework and is
    not involved anyhow in the project's contracts.

**Note** that several updates have been made since the review time and several
issues listed in this review have been fixed. Such issues will be marked as
"fixed in [latest release]".

The token contract is now deployed at
https://etherscan.io/address/0xe2ee1ac57b2e5564522b2de064a47b3f98b0e9c9 and
the crowd sale contract is at
https://etherscan.io/address/0x8B8242Ca04E571B3D03C85be81867cB9300306A6.

[WLproject]: https://github.com/PillarDevelopment/Portfolio_SK/tree/master/WLproject
[latest release]: https://github.com/PillarDevelopment/Portfolio_SK/tree/94d7260b24fb7df90fb97e4deb6f0659eca0ca1b


Overview
========

WhalesburgCrowdsale
-------------------

**Note**, addresses below are updated to what they are in the latest release.

Introduced at WhalesburgSale.sol:56. Crowdsale contract performs the following
public functions:

1.  Selling tokens for the ether (fallback function):
    * 1 ether is worth 0.000'071'8 tokens,
    * caller must be white-listed,
    * total balance raised must not exceed hard cap (although possible to
      exceed, see below),
    * current time is within ICO time: Sunday, 03-Jun-18 16:00:00 UTC -
      Tuesday, 03-Jul-18 16:00:00 UTC,
    * sends received ether to the predefined wallet
      (`0x5dc5c66eb90dd8c4be285164ca9ea442faa1c2e8`),
    * counts total number of buyers,
    * every participant investment must not exceed individual cap calculated
      as following.

    Time from the start | Individual cap
    ------------------- | --------------
    First 2 hours       | 1.25 ether
    Following 2 hours   | 3.75 ether
    Remaining time      | 1365.0000674 ether, the hard cap

2.  Distributes tokens from the given wallet (function `distributionTokens`):
    * initiated by the contract creator,
    * only once in the life of the contract,
    * the distribution sends token to the following addresses:

    Receiver address                             | Token amount | Comment
    -------------------------------------------- | ------------ | -------
    `0x96abf0420cffe408ba6bb16699f6748bef01b02b` |  3'500'000   | Bounty
    `0x44eedeecc2a6f5f763a18e8876576b29a856d03a` | 46'988'857   | Pre ICO
    `0x8e23cd7ce780e55ace7309b398336443b408c9d4` | 20'500'000   | Developers
    `0xd7dadf6149FF75f76f36423CAD1E24c81847E85d` | 10'000'000   | Founders

    Total is 80'988'857 tokens.

    **Note** that the developers address can be updated by the contract
    creator. Other addresses are hard-coded and cannot be changed.

    **Note** that the distribution fails if at the time of the distribution,
    the wallet has no enough tokens. In this case, the distribution can be
    tried again later.

3.  Adds or removes the candidates to or form the white list (functions
    `authorize`, `addManyAuthorizeToWhitelist` and `revoke`):
    * can be done only by the owner at his own discretion.
    * fails if the specified candidate is already in the list.

4.  Finalizes the crowdsale by burning the remaining tokens (function
    `finalize`).
    * Can be done only by the owner at his own discretion.
    * Can be called only once.
    * Can be called either after the ICO time passes of is hard Cap is reached.
    * transfers the remaining token balance to the owner's account.

5.  Updates the developers address for the initial token distribution
    (function `setVestingAddress`).
    * Can be called only by the owner.

TokenVesting
------------

Introduced at VestingConstructor:115. It is a copy of [Open Zeppelin's
TokenVesting] contract, except comments and formatting. It is a token holder
contract that can release its token balance gradually like a typical vesting
scheme, with a cliff and vesting period.

[Open Zeppelin's TokenVesting]: https://github.com/OpenZeppelin/openzeppelin-solidity/blob/ad12381549c4c0711c2f3310e9fb1f65d51c299c/contracts/token/ERC20/TokenVesting.sol


VestingCreator
--------------

Introduced in VestingConstructor.sol:201. The constructor is a factory of
TokenVesting contracts (see above).

The contract exposes the following public functions:

1.  It's method `createVesting`:
    * can be called only by the owner of the contract,
    * creates new TokenVesting with the specified parameters (beneficiary,
      start of the vesting, cliff, duration and revocable flag)
    * transfers specified amount of tokens from VestingCreator's account to the
      created contract,
    * created contract is accessible through `vestingToken` public function.

2. Transfer ownership with `transferOwnership`:
    * may be called only by the owner,
    * switches the owner to the specified one.


WhalesburgToken
---------------

Introduced in WhalesBurgToken.sol:180. Represents Standard burnable and
pausable ERC20 token.
* named "WhalesburgToken" with symbol "WBT",
* denomination is 18 valuable signs after comma, common for ERC20 tokens,
* initial supply is 100'000'000 tokens granted to the owner upon creation.

The contract exposes the following public functions:

1.  Pause and unpause the token with `pause` and `unpause` calls:
    * contract is initially unpaused,
    * may be called only by the owner,

2.  Standard ERC20 functions: `transfer`, `transferFrom`, `approve`,
    `increaseApproval`, `decreaseApproval`:
    * works only if the token contract is not paused, throws otherwise,
    * calls the implementation from StandardToken Open Zepellin contract.

3.  Burn the tokens with function `burn`:
    * burns specified number of tokens from the caller account,
    * also decreases the total supply by the same number of tokens.

4.  Set the crowdsale contract address with `setSaleAddress`:
    * may be called only by the owner.

5.  Transfer tokens from the crowdsale contract account with
    `transferFromICO`:
    * may be called only by the crowdsale contract (set above),
    * acts the same as `transfer`, but works even if the contract is paused.

6.  Transfer ownership with `transferOwnership`:
    * may be called only by the owner,
    * switches the owner to the specified one.


SafeMath in WhalesburgSale.sol
------------------------------

Introduced at WhalesburgSale.sol:5 and is a library contract. The contract
introduces additional checks for basic mathematical operations (`+`, `-`, `*`
and `/`) preventing against the overflow errors. It is used in
WhalesburgCrowdsale contract.

The contract is a slightly modified version of [Open Zeppelin's SafeMath]
contract.


[Open Zeppelin's SafeMath]: https://github.com/OpenZeppelin/openzeppelin-solidity/blob/master/contracts/math/SafeMath.sol


Ownable in WhalesburgSale.sol
-----------------------------

Introduced at WhalesburgSale.sol:40. Keeps track of the owner of the
contract. The owner is an address who created the contract. Used in the
WhalesburgCrowdsale contract.

Unlike [Open Zeppelin's Ownable] contract, this one does not provide the
ability to change the owner (which may be handy if the owner private key is
compromised and needs to be replaced).

[Open Zeppelin's Ownable]: https://github.com/OpenZeppelin/openzeppelin-solidity/blob/master/contracts/ownership/Ownable.sol


Open Zeppelin Contracts
-----------------------

Following utility and ERC20 helper contracts are copies taken from [Open
Zeppelin v1.9] contracts with minor modifications, formatting and comments
changes:
* SafeMath at VestingConstructor:83,
* Ownable at VestingConstructor.sol:56,
* ERC20Basic at VestingConstructor.sol:6,
* ERC20 at VestingConstructor.sol:16,
* SafeERC20 at VestingConstructor.sol:34.


[Open Zeppelin v1.9]: https://github.com/OpenZeppelin/openzeppelin-solidity/tree/v1.9.0/contracts


Errors
======

Not discovered.


Warnings
========

Although not critical, following issues may cause inconveniences when working
with contracts.

1.  WhalesburgSale.sol:36 `transferFromICO`

    The functions are not in [ERC20 standard]. Putting it to `ERC20` interface
    makes the reader of the code think that any ERC20 compatible token will
    work. This assumption would be false.

2.  WhalesburgSale.sol:97 `event Authorized()`

    The event is emitted after successful `authorize` function call, but not
    after `addManyAuthorizeToWhitelist`. This means that automatic scanning of
    the events may not reflect actual white listers. In addition to
    `tokenHolders` array is not public, the actual white list may not be
    recoverable.

3.  WhalesburgSale.sol:167 `multisig.transfer(msg.value)`

    [ERC20 standard] governs to check for function results. I recommend to use
    `require()` here.


Low severity warnings
=====================

Following are instances highlight minor issues that are likely to stay
unnoticed.

1.  WhalesburgSale.sol:134 `require(whitelist[_beneficiaries[i]] != true)`

    If the `_beneficiaries` array contains any member who is already
    white-listed, the whole function would throw (and the gas lost).

2.  WhalesburgSale.sol:79 `uint256 public investors`

    The public variable actually reflects buyers of the crowdsale. Actual
    number of investors may be bigger considering initial distribution
    beneficiaries or those who bought tokens without the crowdsale.

3.  WhalesburgSale.sol:93 `address[] tokenHolders`

    Arrays are expensive in smart contracts (in terms of gas
    consumption). Moreover, it is only used for `investors` calculation and
    not visible (public) itself. I recommend removing it and fixing
    `investors` calculations (see above).

4.  WhalesburgSale.sol:151 `weisRaised > hardCap`

    **Note**, this issue is fixed in the latest release.

    If the current balance equals exactly the hard cap and the ICO time is not
    up, it is not possible to either `finalize` or buy more tokens. I
    recommend to use `>=` here.

5.  WhalesburgSale.sol:159 `function () isUnderHardCap public payable`

    It is possible to exceed the hard cap if the last payment exceeds it.

6.  WhalesburgSale.sol:162 `now > startICO`

    **Note**, this issue is fixed in the latest release.

    Use `>=` here. If these lines are ever (in future updates) used from
    constructor context, it will revert. Same above.

7.  WhalesburgSale.sol:178 `now >= startICO.add(7200)`
    WhalesburgSale.sol:182 `now >= startICO.add(14400)`
    WhalesburgSale.sol:186 `now >= startICO.add(86400)`

    Redundant condition checks (waste of gas) after `else`. Moreover, no need
    to use SafeMath `.add` here.

8.  WhalesburgSale.sol:196 `amount.mul(1e18).div(buyPrice)`

    If `amount` is large enough, it will overflow and assert in `mul()`.  I
    recommend storing `buyPrice` as a normalized value. Currently it is
    71'800'000'000'000, then `buyPrice` could be just 718 and the
    original expression could be `amount.mul(1e7).div(buyPrice)` saving precision.

9.  VestingConstructor.sol:209: `bool public revocable`

    The variable only shows if the last created TokenVesting is
    revocable. When the next TokenVesting is created, the variable is reset to
    reflect its state. This information can be obtained from `vestingToken`
    itself. So I was not sure if this variable is reasonable here, although
    not harmful, of cause.

10. VestingConstructor.sol:217 `function tokenBalance() public returns (uint256 balance)`

    **Note**, this issue is fixed in the latest release.

    The function is useless: it does not change the state and is not constant.

11. WhalesBurgToken.sol:126 `uint256 constant public decimals`

    [ERC20 standard] declares this variable should be `uint8`.


Recommendations
===============

1.  WhalesburgSale.sol:163 `require(moneySpent[msg.sender] <= individualRoundCap)`

    Investor sending money has a chance to see the transaction failed just
    because he overused his individual cap at this time. This does not feel
    nice to investor. Best practices here is to return excess money and
    probably fire a warning event.

2.  Follow the [solidity style guide].

3.  Put constants aside of the code (`currentSaleLimit`). So they are more
    visible and manageable. I recommend using `seconds` | `minutes` | `hours`
    | `days` | `ether` specifiers to make constants clear and less
    error-prone.

4.  Mix of `uint` and `uint256` types. It is better to stick to either style.

5.  Add and run tests. The bigger coverage the better.


Conclusion
==========

No critical or high severity issues were found. A number of recommendations
were proposed to follow best practices and reduce potential attack surface.


Signature
=========

The markdown original of this document is signed with the PGP key fingerprint
57DE 505D A5BB 4D8B C878 50B8 96B1 ED22 26E4 14FF, the signature is in
[readme.asc](./readme.asc).


[ERC20 standard]: https://eips.ethereum.org/EIPS/eip-20
[solidity style guide]: http://solidity.readthedocs.io/en/develop/style-guide.html
